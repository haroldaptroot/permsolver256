// PermSolver256.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "SAT.hpp"

#include <iostream>
#include <array>
#include <vector>
#include <iomanip>
#include "Solver.h"


using namespace Minisat;
using std::array;

struct AbstractBit
{
    // An abstract bit (eg a bit of the result)
    // is a linear combination of any subset of the 256 input bits
    // If the corresponding SAT-variable is true, then that 
    // bit of the input is in the set.
    Lit bit[256];
};

Lit True;

Lit cnf_and(Solver& s, Lit a, Lit b)
{
    Lit v = mkLit(s.newVar());
    s.addClause(~v, a);
    s.addClause(~v, b);
    s.addClause(v, ~a, ~b);
    return v;
}

Lit cnf_xor(Solver& s, Lit a, Lit b)
{
    Lit v = mkLit(s.newVar());
    s.addClause(~v, ~a, ~b);
    s.addClause(~v, a, b);
    s.addClause(v, ~a, b);
    s.addClause(v, a, ~b);
    return v;
}

// multiply a single row of a constant bitmatrix by a variable 8x8 bitmatrix
array<AbstractBit, 8> BitmatixByByte(Solver& s, array<Lit, 8> src1byte, array<AbstractBit, 64>& X)
{
    array<AbstractBit, 8> res;

    for (size_t i = 0; i < 8; i++)
    {
        for (size_t k = 0; k < 256; k++)
        {
            Lit vv;
            for (size_t j = 0; j < 8; j++)
            {
                Lit t = cnf_and(s, src1byte[j], X[(7 - i) * 8 + j].bit[k]);
                if (j == 0)
                    vv = t;
                else
                    vv = cnf_xor(s, vv, t);
            }
            res[i].bit[k] = vv;
        }
    }

    return res;
}

// multiply a constant bitmatrix by a variable bitmatrix
array<AbstractBit, 64> BitmatrixMultiplyL(Solver& s, array<Lit, 64>& A, array<AbstractBit, 64>& X)
{
    array<AbstractBit, 64> res;

    for (size_t i = 0; i < 8; i++)
    {
        array<Lit, 8> row;
        std::copy(&A[i * 8], &A[i * 8] + 8, &row[0]);
        auto linear_combination_row = BitmatixByByte(s, row, X);
        std::copy(linear_combination_row.begin(), linear_combination_row.end(), &res[i * 8]);
    }

    return res;
}

// "left" GF2P8AFFINEQB (constant on the left, variable on the right) for 256-bit vector
array<AbstractBit, 256> GF2P8AFF_L_256(Solver& s, array<Lit, 64>& A, array<AbstractBit, 256>& X)
{
    array<AbstractBit, 256> res;
    for (size_t i = 0; i < 256; i += 64)
    {
        array<AbstractBit, 64> t;
        std::copy(&X[i], &X[i] + 64, &t[0]);
        t = BitmatrixMultiplyL(s, A, t);
        std::copy(&t[0], &t[0] + 64, &res[i]);
    }
    return res;
}

array<AbstractBit, 256> PERMB(Solver& s, array<Lit, 32*32>& M, array<AbstractBit, 256>& X)
{
    array<AbstractBit, 256> res;
    for (size_t i = 0; i < 256; i += 8)
    {
        size_t ix = i / 8;
        for (size_t j = 0; j < 8; j++)
        {
            for (size_t k = 0; k < 256; k++)
            {
                Lit t = mkLit(s.newVar());
                res[i + j].bit[k] = t;
                for (size_t l = 0; l < 32; l++)
                {
                    s.addClause(~M[ix * 32 + l], ~X[l * 8 + j].bit[k], t);
                    s.addClause(~M[ix * 32 + l], X[l * 8 + j].bit[k], ~t);
                }
            }
        }
    }
    return res;
}

void ForceBitPermutationMask(Solver& s, array<Lit, 64>& Mask)
{
    // Require that exactly one bit is set in each column and in each row of the 8x8 matrix
    for (size_t i = 0; i < 64; i += 8)
    {
        vec<Lit> cl;
        for (size_t j = 0; j < 8; j++)
        {
            for (size_t k = 0; k < j; k++)
            {
                s.addClause(~Mask[i + j], ~Mask[i + k]);
            }
            cl.push(Mask[i + j]);
        }
        s.addClause(cl);
    }
    for (size_t i = 0; i < 8; i++)
    {
        for (size_t j = 0; j < 8; j++)
        {
            for (size_t k = 0; k < j; k++)
            {
                s.addClause(~Mask[i + j * 8], ~Mask[i + k * 8]);
            }
        }
    }
}

void ForceBytePermutationMask(Solver& s, array<Lit, 32*32>& Mask)
{
    for (size_t i = 0; i < 32*32; i += 32)
    {
        vec<Lit> cl;
        for (size_t j = 0; j < 32; j++)
        {
            for (size_t k = 0; k < j; k++)
            {
                s.addClause(~Mask[i + j], ~Mask[i + k]);
            }
            cl.push(Mask[i + j]);
        }
        s.addClause(cl);
    }
    for (size_t i = 0; i < 32; i++)
    {
        for (size_t j = 0; j < 32; j++)
        {
            for (size_t k = 0; k < j; k++)
            {
                s.addClause(~Mask[i + j * 32], ~Mask[i + k * 32]);
            }
        }
    }
}

void ForceBytePermutationMaskPSHUFB(Solver& s, array<Lit, 32 * 32>& Mask)
{
    // A shuffle mask for PSHUFB has all the constraints of a PERMB mask, plus:
    // - the low 16 elements must choose from the low 16 elements of the input
    // - the high 16 elements must choose form the high 16 elements of the input
    // Here we also choose to enforce the constraint:
    // - the same permutation is applied to the low 16 bytes and the high 16 bytes
    // That constraint is not necessary, but enables storing the permutation in 16 bytes.
    // The extra constraints must be added before the basic ForceBytePermutationMask constraints,
    // because soem variables are overwritten.
    for (size_t i = 0; i < 16; i++)
    {
        for (size_t j = 0; j < 16; j++)
        {            
            Mask[i * 32 + j + 16] = ~True;
            Mask[(i + 16) * 32 + j] = ~True;
            Mask[(i + 16) * 32 + (j + 16)] = Mask[i * 32 + j];
        }
    }
    ForceBytePermutationMask(s, Mask); (s, Mask);
}

bool getModel(Solver &s, Lit l)
{
    return s.modelValue(l) == l_True;
}

int main()
{
    Solver s;
    True = mkLit(s.newVar());
    s.addClause(True);

    // Initialize starting state to the identity (i'th element of the input has the value of the i'th bit of the input, obviously)
    array<AbstractBit, 256> Input;
    for (size_t i = 0; i < 256; i++)
    {
        std::fill_n(&Input[i].bit[0], 256, ~True);
        Input[i].bit[i] = True;
    }

    // create variables for the first mask (for PERMB)
    // and force them to represent a byte-permutation
    array<Lit, 32*32> Mask1;
    for (size_t i = 0; i < 32*32; i++)
    {
        Mask1[i] = mkLit(s.newVar());
    }
    ForceBytePermutationMask(s, Mask1);

    // create variables for the second mask (for a left-GF2P8AFFINE)
    // and force them to represent a bit-permutation
    array<Lit, 64> Mask2;
    for (size_t i = 0; i < 64; i++)
    {
        Mask2[i] = mkLit(s.newVar());
    }
    ForceBitPermutationMask(s, Mask2);

    array<Lit, 32 * 32> Mask3;
    for (size_t i = 0; i < 32 * 32; i++)
    {
        Mask3[i] = mkLit(s.newVar());
    }
    ForceBytePermutationMaskPSHUFB(s, Mask3);

    // apply the skeleton program to the abstract input
    auto R2 = PERMB(s, Mask1, Input);
    auto R3 = GF2P8AFF_L_256(s, Mask2, R2);
    auto R4 = PERMB(s, Mask3, R3);
    auto R = R4;

    // require that the output is the transpose of the input
    for (size_t i = 0; i < 16; i++)
    {
        for (size_t j = 0; j < 16; j++)
        {
            for (size_t k = 0; k < 256; k++)
            {
                if (k == (16 * j + i))
                    s.addClause(R[i * 16 + j].bit[k]);
                else
                    s.addClause(~R[i * 16 + j].bit[k]);
            }
        }
    }

    if (s.solve()) {
        std::cout << "__m256i t0 = _mm256_permutexvar_epi8(_mm256_setr_epi8(\n";
        for (size_t i = 0; i < 32; i++)
        {
            size_t idx = 0;
            for (size_t j = 0; j < 32; j++)
            {
                if (getModel(s, Mask1[i * 32 + j])) {
                    idx = j;
                    break;
                }
            }
            if (i) {
                if ((i & 7) == 0)
                    std::cout << ",\n";
                else
                    std::cout << ", ";
            }
            std::cout << idx;
        }
        std::cout << "), input);\n";
        uint64_t gf2paffinemask = 0;
        for (size_t i = 0; i < 64; i++)
        {
            if (getModel(s, Mask2[i]))
                gf2paffinemask |= 1ULL << i;
        }
        std::cout << "__m256i t1 = _mm256_gf2p8affine_epi64_epi8(_mm256_set1_epi64x(" << 
            std::hex << std::setfill('0') << std::setw(16) << gf2paffinemask << std::dec << 
            "), t0, 0);\n";
        std::cout << "__m256i t2 = _mm256_shuffle_epi8(t1, _mm256_setr_epi8(\n";
        for (size_t i = 0; i < 32; i++)
        {
            int idx = -1;
            for (size_t j = 0; j < 32; j++)
            {
                if (getModel(s, Mask3[i * 32 + j]))
                {
                    idx = j;
                    break;
                }
            }
            if (i) {
                if ((i & 7) == 0)
                    std::cout << ",\n";
                else
                    std::cout << ", ";
            }
            // this is a PSHUFB, but indexes go from 0 to 31
            // print them as 0 to 15 by masking with 15
            std::cout << (idx & 15);
        }
        std::cout << "));\n";
    }
    else {
        std::cout << "No model\n";
        return 0;
    }

    std::cout << "Done\n";
}

